/**
 * Created by Theofanis on 24/6/17.
 */

/**
 *
 * @constructor
 */
function GreeklishTranslator() {

}
GreeklishTranslator.GR_REGEX = "[α-ωΑ-Ωάέόώίϊΐήύϋΰ]";
GreeklishTranslator.EN_REGEX = "[a-zA-Z]";

GreeklishTranslator.gr_en = {
    'Α': 'A',
    'Β': 'B',
    'Ψ': 'C',
    'Δ': 'D',
    'Ε': 'E',
    'Φ': 'F',
    'Γ': 'G',
    'Η': 'H',
    'Ι': 'I',
    'Ξ': 'J',
    'Κ': 'K',
    'Λ': 'L',
    'Μ': 'M',
    'Ν': 'N',
    'Ο': 'O',
    'Π': 'P',
    'Ρ': 'R',
    'Σ': '[SW]',
    'Τ': 'T',
    'Θ': 'U',
    'Ω': 'V',
    'Χ': 'X',
    'Υ': 'Y',
    'Ζ': 'Z',
    '' : 'Q',
    'ς': 's',
    // 'ς '  : 's ',
    // 'ς\\n': 's\\n',
    // 'ς\\t': 's\\t'
};

GreeklishTranslator.en_gr = {
    'A': 'Α',
    'B': 'Β',
    'C': 'Ψ',
    'D': 'Δ',
    'E': 'Ε',
    'F': 'Φ',
    'G': 'Γ',
    'H': 'Η',
    'I': 'Ι',
    'J': 'Ξ',
    'K': 'Κ',
    'L': 'Λ',
    'M': 'Μ',
    'N': 'Ν',
    'O': 'Ο',
    'P': 'Π',
    'Q': '',
    'R': 'Ρ',
    'S': 'Σ',
    'T': 'Τ',
    'U': 'Θ',
    'V': 'Ω',
    'W': '',
    'X': 'Χ',
    'Y': 'Υ',
    'Z': 'Ζ',
    'S': 'ς',
    // 'ς '  : 's ',
    // 'ς\\n': 's\\n',
    // 'ς\\t': 's\\t'
};

/**
 *
 * @param {String} text
 * @returns {String}
 * @param {Object} mapper
 */
GreeklishTranslator.prototype.translate = function (text, mapper) {
    var str = text.removeAccents();
    for (var left in mapper) {
        if (mapper[left] === '')
            continue;
        str = str.replace(new RegExp(mapper[left].toUpperCase(), "g"), left.toUpperCase());
        str = str.replace(new RegExp(mapper[left].toLowerCase(), "g"), left.toLowerCase());
    }
    return str;
};

/*####################################################################################################################*/
String.prototype.removeAccents = function () {
    var map = {
        'α': "ά",
        'ε': "έ",
        'η': "ή",
        'ι': "[ίϊΐ]",
        'ο': "ό",
        'υ': "[ύϋΰ]",
        'ω': "ώ"
    };
    var str = this;
    for (var simple in map) {
        str = str.replace(new RegExp(map[simple].toUpperCase(), "g"), simple.toUpperCase());
        str = str.replace(new RegExp(map[simple].toLowerCase(), "g"), simple.toLowerCase());
    }
    return str;
};

String.prototype.removeChars = function (chars) {
    chars = chars instanceof Array ? "[" + chars.join('') + "]" : chars;
    return this.replace(new RegExp(chars, "gi"), '');
};

String.prototype.translate = function (mapper) {
    var str = this;
    for (var left in mapper) {
        if (mapper[left] === '')
            continue;
        str = str.replace(new RegExp(mapper[left].toUpperCase(), "g"), left.toUpperCase());
        str = str.replace(new RegExp(mapper[left].toLowerCase(), "g"), left.toLowerCase());
    }
    return str;
};