Template7.global = {
    android   : Framework7.prototype.device.android === true,
    ios       : Framework7.prototype.device.ios === true,
    appName   : "Greeklish Translator",
    ok        : "OK",
    cancel    : "Άκυρο",
    loading   : "Φόρτωση...",
    iTunes    : "https://itunes.apple.com/gr/app/%CF%83%CE%AE%CE%BC%CE%B1%CF%84%CE%B1-drivers-quiz/id1205139762",
    GooglePlay: "https://play.google.com/store/apps/details?id=tech.theograms.driversquiz",
    AppStore  : "itms-apps://itunes.apple.com/app/id1205139762",
    PlayStore : "market://details?id=tech.theograms.driversquiz",
};
Template7.global.appstore = Framework7.prototype.device.ios ? Template7.global.AppStore : Template7.global.PlayStore;
Template7.global.shareAppTitle = 'Νέα εφαρμογή "' + Template7.global.appName + '"';
Template7.global.shareAppMessage =
    'Δες την εφαρμογή "' + Template7.global.appName + '".\n' +
    'Υπολογίζει ποια βάρδια θα έχεις σε κάθε ημερομηνία.\n\n' +
    'Είναι διαθέσιμη για iOS\n' +
    Template7.global.iTunes + '\n\n' +
    'και Android\n' +
    Template7.global.GooglePlay;

var myApp = new Framework7({
    init                 : false,
    precompileTemplates  : true,
    material             : Framework7.prototype.device.android,
    smartSelectOpenIn    : 'picker',
    template7Pages       : true,
    modalTitle           : Template7.global.appName,
    modalButtonOk        : Template7.global.ok,
    modalButtonCancel    : Template7.global.cancel,
    modalPreloaderTitle  : Template7.global.loading,
    pushState            : false, // #! on browser
    tapHold              : false,
    cache                : false,
    onAjaxStart          : function (xhr) {
        //myApp.showIndicator();
    },
    onAjaxComplete       : function (xhr) {
        //myApp.hideIndicator();
    },
    template7Data        : {
        "page:index": {}
    },
    onPageBeforeInit     : function (app, page) {
        console.log('page before init ' + page.name);
        if (Framework7.prototype.device.android) {
            $$('.navbar-through').addClass('navbar-fixed').removeClass('navbar-through');
        }
    },
    onPageInit           : function (app, page) {
        console.log('page init ' + page.name);
    },
    onPageReinit         : function (app, page) {
        console.log('page before reinit ' + page.name);
    },
    onPageBeforeAnimation: function (app, page) {
        console.log('page before animation ' + page.name);
        myApp.sizeNavbars();
    },
    onPageBeforeRemove   : function (app, page) {
        console.log('page before remove ' + page.name);
    },
    onPageBack           : function (app, page) {
        console.log('page back ' + page.name);
    },
    onPageAfterBack      : function (app, page) {
        console.log('page after back ' + page.name);
    }
});

var $$ = Dom7;

$$.fn.removeClassPrefix = function (prefix) {
    this.each(function (i, el) {
        el.className = el.className.split(/\s+/).filter(function (c) {
            return c.lastIndexOf(prefix, 0) !== 0;
        }).join(" ").trim();
    });
    return this;
};

if (Framework7.prototype.device.android) {
    $$('.view .navbar').prependTo('.view .page');
    $$('head').append(
        '<link rel="stylesheet" href="lib/framework7/css/framework7.material.min.css">'
        + '<link rel="stylesheet" href="lib/framework7/css/framework7.material.colors.min.css">'
        + '<link rel="stylesheet" href="css/my-app.material.css">'
    );
} else {
    $$('head').append(
        '<link rel="stylesheet" href="css/my-app.ios.css">'
        + '<link rel="stylesheet" href="lib/framework7/css/framework7.ios.min.css">'
        + '<link rel="stylesheet" href="lib/framework7/css/framework7.ios.colors.min.css">'
    );
}

var mainView = myApp.addView('.view-main', {
    dynamicNavbar: Framework7.prototype.device.ios,
    url          : "index.html"
});

translator = new GreeklishTranslator();

$$(document).on('deviceready', function () {
    console.log("Device is ready!");
    new Clipboard('.clipboard');
    moment.locale('el');
    myApp.init();
    mainView.refreshPage();
    console.log("myApp initialized on " + myApp.device.os);
});

// myApp.onPageBeforeInit('index', function (page) {
//     var inputForm = $$(page.container).find('form.card.input');
//     var outputForm = $$(page.container).find('form.card.output');
//     var input = myApp.formGetData('input');
//     if (input)
//         myApp.formFromData(inputForm, input);
//     var output = myApp.formGetData('output');
//     if (output)
//         myApp.formFromData(outputForm, output);
// });

myApp.onPageInit('index', function (page) {
    var inputForm = $$(page.container).find('form.card.input');
    var outputForm = $$(page.container).find('form.card.output');
    var input = $$(inputForm).find('textarea.input');
    var output = $$(outputForm).find('textarea.output');
    var inputCapitalise, outputCapitalise;
    var inputRestrict, outputRestrict;
    $$(inputForm).on('input change', function () {
        myApp.formStoreData('input', myApp.formToData(this));
    });
    $$(outputForm).on('input change', function () {
        myApp.formStoreData('output', myApp.formToData(this));
    });
    input.on('change keyup paste', function () {
        if (inputRestrict)
            $$(this).val($$(this).val().removeChars(GreeklishTranslator.GR_REGEX));
        if (inputCapitalise)
            $$(this).val($$(this).val().toUpperCase());
        $$(output).val($$(this).val().translate(GreeklishTranslator.gr_en));
    });
    output.on('change keyup paste', function () {
        if (outputRestrict)
            $$(this).val($$(this).val().removeChars(GreeklishTranslator.EN_REGEX));
        if (outputCapitalise)
            $$(this).val($$(this).val().toUpperCase());
        input.val($$(this).val().removeAccents().translate(GreeklishTranslator.en_gr));
    });
    $$(inputForm).find('input[type=checkbox][name=capitalise]').change(function () {
        var formData = myApp.formToData(inputForm);
        inputCapitalise = formData.capitalise.length;
        input.change();
    });
    $$(outputForm).find('input[type=checkbox][name=capitalise]').change(function () {
        var formData = myApp.formToData(outputForm);
        outputCapitalise = formData.capitalise.length;
        output.change();
    });
    $$(inputForm).find('input[type=checkbox][name=restrict]').change(function () {
        var formData = myApp.formToData('.card.input');
        inputRestrict = formData.restrict.length;
        input.change();
    });
    $$(outputForm).find('input[type=checkbox][name=restrict]').change(function () {
        var formData = myApp.formToData('.card.output');
        outputRestrict = formData.restrict.length;
        output.change();
    });
    $$(inputForm).find('a.button.clear').click(function () {
        input.val('');
        // input.change();
    });
    $$(outputForm).find('a.button.clear').click(function () {
        output.val('');
        // output.change();
    });
    {
        var inputData = myApp.formGetData('input');
        if (inputData) {
            myApp.formFromData(inputForm, inputData);
            inputCapitalise = inputData.capitalise.length;
            inputRestrict = inputData.restrict.length;
            // input.change();
        }
        var outputData = myApp.formGetData('output');
        if (outputData) {
            myApp.formFromData(outputForm, outputData);
            outputCapitalise = outputData.capitalise.length;
            outputRestrict = outputData.restrict.length;
            // output.change();
        }
    }
});
